######################################################################
#
#	As of 2.0.0, FreeRADIUS supports virtual hosts using the
#	"server" section, and configuration directives.
#
#	Virtual hosts should be put into the "sites-available"
#	directory.  Soft links should be created in the "sites-enabled"
#	directory to these files.  This is done in a normal installation.
#
######################################################################
#
#	Read "man radiusd" before editing this file.  See the section
#	titled DEBUGGING.  It outlines a method where you can quickly
#	obtain the configuration you want, without running into
#	trouble.  See also "man unlang", which documents the format
#	of this file.
#
######################################################################

server eduroam {

listen {
	type = auth

	ipv4addr = *
	ipv6addr = *

	port = 0

#	interface = eth0
#	clients = per_socket_clients

	limit {
	      max_connections = 16
	      lifetime = 0
	      idle_timeout = 30
	}
}

listen {
	type = acct

	ipv4addr = *
	ipv6addr = *

	port = 0

#	interface = eth0
#	clients = per_socket_clients

	limit {
#		max_pps = 0
#		idle_timeout = 0
#		lifetime = 0
#		max_connections = 0
	}
}

#
#  Authorization
#
authorize {

	#
	# Filtering for bad eduroam User-Name format
	$INCLUDE ${confdir}/eduroam-realm-checks.conf

	#
	# eduroam Monitoring Check
	$INCLUDE ${confdir}/eduroam-mon-checks.conf

	#
	# The "sufffix" module takes care of stripping the domain
	# (e.g. "@example.com") from the User-Name attribute.
	preprocess
	suffix

	#
	# Change realm to be LOCAL for local user
	if( ("%{Realm}" =~ /u21.ac.th$$/) ) {
		if( ("%{Realm}" =~ /^u21.ac.th$$/) ) {
			update control {
				Proxy-To-Realm := LOCAL
			}
		}
	}
	else {
		update request {
			Realm := "eduroam"
		}
		update control {
			Proxy-To-Realm := "eduroam"
		}
	}

	#
	#  default log from radius
	auth_log

	#
	#  This module takes care of EAP-MD5, EAP-TLS, and EAP-LEAP
	#  authentication.
	eap-eduroam {
		ok = return
	}

	#
	#  If "status_server = yes", then Status-Server messages are passed
	#  through the following section, and ONLY the following section.
	Autz-Type Status-Server {
		ok
	}

}

#
#  Authentication
#
authenticate {

	#
	#  Most people want CHAP authentication
	Auth-Type CHAP {
		chap
	}

	#
	#  MSCHAP authentication.
	Auth-Type MS-CHAP {
		mschap
	}

	#
	# Original EAP authentication.
	Auth-Type EAP {
		eap
	}

	#
	#  Allow EAP authentication.
	eap-eduroam

}

#
#  Pre-accounting
#
preacct {

	preprocess
	suffix

}

#
#  Accounting
#
accounting {

	#
	#  Create a 'detail'ed log of the packets.
	detail

	#radutmp

	#  Filter attributes from the accounting response.
	attr_filter.accounting_response

	Acct-Type Status-Server {

	}

}

#
#  Session
#
session {

	#radutmp

}

#
#  Post-Authentication
#
post-auth {

	#
	#  For EAP-TTLS and PEAP, add the cached attributes to the reply.
	update {
		&reply: += &session-state:
	}

	#
	#  default log from radius
	reply_log

	#  Remove reply message if the response contains an EAP-Message
	remove_reply_message_if_eap

	#
	#  Access-Reject packets are sent through the REJECT sub-section of the
	#  post-auth section.
	Post-Auth-Type REJECT {
		attr_filter.access_reject
	}

}

#
#  Pre-proxy
#
pre-proxy {

	#
	# Update Operator-Name to IdP
	if ("%{Operator-Name}" == "") {
		update proxy-request {
			Operator-Name := "1u21.ac.th"
		}
	}

	#
	#  Filter requests sent to remote servers based on the rules
	#  defined in the 'attrs.pre-proxy' file.
	if("%{Packet-Type}" != "Accounting-Request") {
		attr_filter.pre-proxy
	}

	#
	#  Log of packets proxied to a home server.
	pre_proxy_log

}

#
#  Post-proxy
#
post-proxy {

	#
	#  Log of replies from a home server.
	post_proxy_log

}

} # server eduroam {}
