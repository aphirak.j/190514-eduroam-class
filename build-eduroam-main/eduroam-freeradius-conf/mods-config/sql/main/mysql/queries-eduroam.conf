# -*- text -*-
#
#  main/mysql/queries.conf-- MySQL configuration for default schema (schema.sql)
#
#  $Id: 40508024d5fd6a319bbb85775c3fe1e8388be656 $

# Safe characters list for sql queries.
#safe_characters = "@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_: /"

#######################################################################
#  Connection config
#######################################################################
# The character set is not configurable.
# Create/edit my.cnf (typically in /etc/mysql/my.cnf or /etc/my.cnf)
# and enter
# [client]
# default-character-set = utf8
#

#######################################################################
#  Query config:  Username
#######################################################################
# This is the username that will get substituted, escaped, and added
# as attribute 'SQL-User-Name'. '%{SQL-User-Name}' should be used below
# everywhere a username substitution is needed so you you can be sure
# the username passed from the client is escaped properly.
#
sql_user_name = "%{%{Stripped-User-Name}:-%{%{User-Name}:-DEFAULT}}"
#
#sql_user_name = "%{%{User-Name}"

#######################################################################
# Default profile
#######################################################################
#default_user_profile = "DEFAULT"

#######################################################################
# NAS Query
#######################################################################
#
# 0. Row ID (currently unused)
# 1. Name (or IP address)
# 2. Shortname
# 3. Type
# 4. Secret
# 5. Server
#######################################################################

client_query = "\
	SELECT id, nasname, shortname, type, secret, server \
	FROM ${client_table}"

#######################################################################
# Authorization Queries
#######################################################################
#
# 0. Row ID (currently unused)
# 1. UserName/GroupName
# 2. Item Attr Name
# 3. Item Attr Value
# 4. Item Attr Operation
#######################################################################
# Use these for case sensitive usernames.

#authorize_check_query = "\
#	SELECT id, username, attribute, value, op \
#	FROM ${authcheck_table} \
#	WHERE username = BINARY '%{SQL-User-Name}' \
#	ORDER BY id"

#authorize_reply_query = "\
#	SELECT id, username, attribute, value, op \
#	FROM ${authreply_table} \
#	WHERE username = BINARY '%{SQL-User-Name}' \
#	ORDER BY id"

#
#  The default queries are case insensitive. (for compatibility with
#  older versions of FreeRADIUS)
#
authorize_check_query = "\
	SELECT id, username, attribute, value, op \
	FROM ${authcheck_table} \
	WHERE username = '%{SQL-User-Name}' \
	ORDER BY id"

authorize_reply_query = "\
	SELECT id, username, attribute, value, op \
	FROM ${authreply_table} \
	WHERE username = '%{SQL-User-Name}' \
	ORDER BY id"

#
#  Use these for case sensitive usernames.
#
#group_membership_query = "\
#	SELECT groupname \
#	FROM ${usergroup_table} \
#	WHERE username = BINARY '%{SQL-User-Name}' \
#	ORDER BY priority"

#group_membership_query = "\
#	SELECT groupname \
#	FROM ${usergroup_table} \
#	WHERE username = '%{SQL-User-Name}' \
#	ORDER BY priority"

#authorize_group_check_query = "\
#	SELECT id, groupname, attribute, \
#	Value, op \
#	FROM ${groupcheck_table} \
#	WHERE groupname = '%{${group_attribute}}' \
#	ORDER BY id"

#authorize_group_reply_query = "\
#	SELECT id, groupname, attribute, \
#	value, op \
#	FROM ${groupreply_table} \
#	WHERE groupname = '%{${group_attribute}}' \
#	ORDER BY id"

#######################################################################
# Simultaneous Use Checking Queries
#######################################################################
# simul_count_query	- query for the number of current connections
#			- If this is not defined, no simultaneous use checking
#			- will be performed by this module instance
# simul_verify_query	- query to return details of current connections
#				for verification
#			- Leave blank or commented out to disable verification step
#			- Note that the returned field order should not be changed.
#######################################################################

simul_count_query = "\
	SELECT COUNT(*) \
	FROM ${acct_table1} \
	WHERE username = '%{SQL-User-Name}' \
	AND acctstoptime IS NULL"

simul_verify_query = "\
	SELECT \
		radacctid, acctsessionid, username, nasipaddress, nasportid, framedipaddress, \
		callingstationid, framedprotocol \
	FROM ${acct_table1} \
	WHERE username = '%{SQL-User-Name}' \
	AND acctstoptime IS NULL"

#######################################################################
# Accounting and Post-Auth Queries
#######################################################################
accounting {
	reference = "%{tolower:type.%{Acct-Status-Type}.query}"

	# Write SQL queries to a logfile. This is potentially useful for bulk inserts
	# when used with the rlm_sql_null driver.
#	logfile = ${logdir}/accounting.sql

	column_list = "\
		acctsessionid,		acctuniqueid,		username, \
		realm,			nasipaddress,		nasportid, \
		nasporttype,		acctstarttime,		acctupdatetime, \
		acctstoptime,		acctsessiontime, 	acctauthentic, \
		connectinfo_start,	connectinfo_stop, 	acctinputoctets, \
		acctoutputoctets,	calledstationid, 	callingstationid, \
		acctterminatecause,	servicetype,		framedprotocol, \
		framedipaddress"

	type {
		accounting-on {
			#
			#  Bulk terminate all sessions associated with a given NAS
			#
			query = "\
				UPDATE ${....acct_table1} \
				SET \
					acctstoptime = FROM_UNIXTIME(\
						%{integer:Event-Timestamp}), \
					acctsessiontime	= '%{integer:Event-Timestamp}' \
						- UNIX_TIMESTAMP(acctstarttime), \
					acctterminatecause = '%{%{Acct-Terminate-Cause}:-NAS-Reboot}' \
				WHERE acctstoptime IS NULL \
				AND nasipaddress   = '%{NAS-IP-Address}' \
				AND acctstarttime <= FROM_UNIXTIME(\
					%{integer:Event-Timestamp})"
		}

		accounting-off {
			query = "${..accounting-on.query}"
		}

		start {
			#
			#  Insert a new record into the sessions table
			#
			query = "\
				INSERT INTO ${....acct_table1} \
					(${...column_list}) \
				VALUES \
					('%{Acct-Session-Id}', \
					'%{Acct-Unique-Session-Id}', \
					'%{SQL-User-Name}', \
					'%{Realm}', \
					'%{NAS-IP-Address}', \
					'%{%{NAS-Port-ID}:-%{NAS-Port}}', \
					'%{NAS-Port-Type}', \
					FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					NULL, \
					'0', \
					'%{Acct-Authentic}', \
					'%{Connect-Info}', \
					'', \
					'0', \
					'0', \
					'%{Called-Station-Id}', \
					'%{Calling-Station-Id}', \
					'', \
					'%{Service-Type}', \
					'%{Framed-Protocol}', \
					'%{Framed-IP-Address}')"

			#
			#  Key constraints prevented us from inserting a new session,
			#  use the alternate query to update an existing session.
			#
			query = "\
				UPDATE ${....acct_table1} SET \
					acctstarttime	= FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					acctupdatetime	= FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					connectinfo_start = '%{Connect-Info}' \
				WHERE AcctUniqueId = '%{Acct-Unique-Session-Id}'"
		}

		interim-update {
			#
			#  Update an existing session and calculate the interval
			#  between the last data we received for the session and this
			#  update. This can be used to find stale sessions.
			#
			query = "\
				UPDATE ${....acct_table1} \
				SET \
					acctupdatetime  = (@acctupdatetime_old:=acctupdatetime), \
					acctupdatetime  = FROM_UNIXTIME(\
						%{integer:Event-Timestamp}), \
					acctinterval    = %{integer:Event-Timestamp} - \
						UNIX_TIMESTAMP(@acctupdatetime_old), \
					framedipaddress = '%{Framed-IP-Address}', \
					acctsessiontime = %{%{Acct-Session-Time}:-NULL}, \
					acctinputoctets = '%{%{Acct-Input-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Input-Octets}:-0}', \
					acctoutputoctets = '%{%{Acct-Output-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Output-Octets}:-0}' \
				WHERE AcctUniqueId = '%{Acct-Unique-Session-Id}'"

			#
			#  The update condition matched no existing sessions. Use
			#  the values provided in the update to create a new session.
			#
			query = "\
				INSERT INTO ${....acct_table1} \
					(${...column_list}) \
				VALUES \
					('%{Acct-Session-Id}', \
					'%{Acct-Unique-Session-Id}', \
					'%{SQL-User-Name}', \
					'%{Realm}', \
					'%{NAS-IP-Address}', \
					'%{%{NAS-Port-ID}:-%{NAS-Port}}', \
					'%{NAS-Port-Type}', \
					FROM_UNIXTIME(%{integer:Event-Timestamp} - %{%{Acct-Session-Time}:-0}), \
					FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					NULL, \
					%{%{Acct-Session-Time}:-NULL}, \
					'%{Acct-Authentic}', \
					'%{Connect-Info}', \
					'', \
					'%{%{Acct-Input-Gigawords}:-0}' << 32 | '%{%{Acct-Input-Octets}:-0}', \
					'%{%{Acct-Output-Gigawords}:-0}' << 32 | '%{%{Acct-Output-Octets}:-0}', \
					'%{Called-Station-Id}', \
					'%{Calling-Station-Id}', \
					'', \
					'%{Service-Type}', \
					'%{Framed-Protocol}', \
					'%{Framed-IP-Address}')"
		}

		stop {
			#
			#  Session has terminated, update the stop time and statistics.
			#
			query = "\
				UPDATE ${....acct_table2} SET \
					acctstoptime	= FROM_UNIXTIME(\
						%{integer:Event-Timestamp}), \
					acctsessiontime	= %{%{Acct-Session-Time}:-NULL}, \
					acctinputoctets	= '%{%{Acct-Input-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Input-Octets}:-0}', \
					acctoutputoctets = '%{%{Acct-Output-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Output-Octets}:-0}', \
					acctterminatecause = '%{Acct-Terminate-Cause}', \
					connectinfo_stop = '%{Connect-Info}' \
				WHERE AcctUniqueId = '%{Acct-Unique-Session-Id}'"

			#
			#  The update condition matched no existing sessions. Use
			#  the values provided in the update to create a new session.
			#
			query = "\
				INSERT INTO ${....acct_table2} \
					(${...column_list}) \
				VALUES \
					('%{Acct-Session-Id}', \
					'%{Acct-Unique-Session-Id}', \
					'%{SQL-User-Name}', \
					'%{Realm}', \
					'%{NAS-IP-Address}', \
					'%{%{NAS-Port-ID}:-%{NAS-Port}}', \
					'%{NAS-Port-Type}', \
					FROM_UNIXTIME(%{integer:Event-Timestamp} - %{%{Acct-Session-Time}:-0}), \
					FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					FROM_UNIXTIME(%{integer:Event-Timestamp}), \
					%{%{Acct-Session-Time}:-NULL}, \
					'%{Acct-Authentic}', \
					'', \
					'%{Connect-Info}', \
					'%{%{Acct-Input-Gigawords}:-0}' << 32 | '%{%{Acct-Input-Octets}:-0}', \
					'%{%{Acct-Output-Gigawords}:-0}' << 32 | '%{%{Acct-Output-Octets}:-0}', \
					'%{Called-Station-Id}', \
					'%{Calling-Station-Id}', \
					'%{Acct-Terminate-Cause}', \
					'%{Service-Type}', \
					'%{Framed-Protocol}', \
					'%{Framed-IP-Address}')"
		}
	}
}


#######################################################################
# Authentication Logging Queries
#######################################################################
# postauth_query	- Insert some info after authentication
#######################################################################

post-auth {
	# Write SQL queries to a logfile. This is potentially useful for bulk inserts
	# when used with the rlm_sql_null driver.
#	logfile = ${logdir}/post-auth.sql

	query =	"\
		INSERT INTO ${..postauth_table} \
			(username, pass, reply, authdate) \
		VALUES ( \
			'%{SQL-User-Name}', \
			'%{%{User-Password}:-%{Chap-Password}}', \
			'%{reply:Packet-Type}', \
			'%S')"
}
